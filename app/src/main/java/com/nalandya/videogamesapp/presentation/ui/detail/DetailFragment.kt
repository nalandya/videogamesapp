package com.nalandya.videogamesapp.presentation.ui.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nalandya.videogamesapp.R
import com.nalandya.videogamesapp.databinding.FragmentDetailVideoGameBinding
import com.nalandya.videogamesapp.presentation.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment :
    BaseFragment<FragmentDetailVideoGameBinding>(R.layout.fragment_detail_video_game) {
    override val viewModel: DetailViewModel by viewModels()

    private val args: DetailFragmentArgs by navArgs()

    override fun onResume() {
        super.onResume()
        viewModel.initRequest(args.videoGamesId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectFlows(
            listOf(::collectUiState)
        )
        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private suspend fun collectUiState() {
        viewModel.uiState.collect { state ->
            if (state.isError) showSnackbar(
                message = state.errorText!!,
                actionText = getString(R.string.button_retry)
            ) {
                viewModel.retryConnection {
                    viewModel.initRequest(args.videoGamesId)
                }
            }
        }
    }


}