package com.nalandya.videogamesapp.presentation.ui.favorite

import androidx.lifecycle.viewModelScope
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.usecase.AddFavorite
import com.nalandya.videogamesapp.domain.usecase.DeleteFavorite
import com.nalandya.videogamesapp.domain.usecase.GetFavorites
import com.nalandya.videogamesapp.presentation.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val getFavorites: GetFavorites,
    private val deleteFavorite: DeleteFavorite,
    private val addFavorite: AddFavorite
) : BaseViewModel() {

    private val _favoriteVideoGames = MutableStateFlow(emptyList<FavoriteVideoGame>())
    val favoriteVideoGames get() = _favoriteVideoGames.asStateFlow()

    fun fetchFavoriteMovies() {
        viewModelScope.launch {
            getFavorites().collect {
                _favoriteVideoGames.value = it
            }
        }
    }

    fun removeFavorite(favoriteVideoGame: FavoriteVideoGame) {
        viewModelScope.launch {
            deleteFavorite(favoriteVideoGame = favoriteVideoGame)
            fetchFavoriteMovies()
        }
    }

    fun addToFavorite(favoriteVideoGame: FavoriteVideoGame) {
        viewModelScope.launch {
            addFavorite(favoriteVideoGame = favoriteVideoGame)
            fetchFavoriteMovies()
        }
    }

    init {
        fetchFavoriteMovies()
    }
}