package com.nalandya.videogamesapp.presentation.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nalandya.videogamesapp.databinding.ItemVideoGamesBinding
import com.nalandya.videogamesapp.domain.model.VideoGame

class VideoGamesAdapter(private val onItemClick: (id: Int) -> Unit) :
    ListAdapter<VideoGame, VideoGamesAdapter.ViewHolder>(DiffCallback) {
    inner class ViewHolder(val view: ItemVideoGamesBinding) :
        RecyclerView.ViewHolder(view.root) {
        constructor(parent: ViewGroup) : this(
            ItemVideoGamesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

        init {
            view.root.setOnClickListener {
                onItemClick(getItem(adapterPosition).id)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VideoGamesAdapter.ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.videoGame = getItem(position)
    }


    object DiffCallback : DiffUtil.ItemCallback<VideoGame>() {
        override fun areItemsTheSame(oldItem: VideoGame, newItem: VideoGame): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: VideoGame, newItem: VideoGame): Boolean =
            oldItem == newItem
    }
}