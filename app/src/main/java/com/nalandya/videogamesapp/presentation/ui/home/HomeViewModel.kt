package com.nalandya.videogamesapp.presentation.ui.home

import androidx.lifecycle.viewModelScope
import com.nalandya.videogamesapp.domain.model.VideoGame
import com.nalandya.videogamesapp.domain.usecase.GetList
import com.nalandya.videogamesapp.domain.usecase.GetSearchResults
import com.nalandya.videogamesapp.presentation.ui.base.BaseViewModel
import com.nalandya.videogamesapp.util.Resource
import com.nalandya.videogamesapp.util.SearchViewListener
import com.nalandya.videogamesapp.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getList: GetList,
    private val getSearchResults: GetSearchResults
) : BaseViewModel() {

    private val _videoGames = MutableStateFlow(emptyList<VideoGame>())
    val videoGames get() = _videoGames.asStateFlow()

    var page = 1
        private set

    var query = ""
    var isQueryChanged = false
        private set

    val searchViewListener = object : SearchViewListener {
        override fun onQueryTextSubmit(query: String) {
            this@HomeViewModel.query = query
            fetchInitialSearch()
        }

        override fun onClear() {
            query = ""
            initRequest()
        }

    }

    init {
        initRequest()
    }

    fun initRequest() {
        page = 1
        isQueryChanged = true
        viewModelScope.launch {
            if (query.isNotEmpty()) {
                fetchSearchResult()
            } else {
                fetchVideoGames()
            }
        }
    }

    fun onLoadMore() {
        _uiState.value = UiState.loadingState()
        isQueryChanged = false
        page++
        viewModelScope.launch {
            if (query.isNotEmpty()) {
                fetchSearchResult()
            } else {
                fetchVideoGames()
            }
        }
    }

    private suspend fun fetchVideoGames() {
        _uiState.value = UiState.loadingState()
        getList(page).collect { response ->
            when (response) {
                is Resource.Success -> {
                    val videoGameList = (response as Resource.Success).data.results
                    _videoGames.value =
                        if (page == 1) videoGameList else _videoGames.value + videoGameList
                    _uiState.value = UiState.successState()
                }

                is Resource.Error -> {
                    _uiState.value = UiState.errorState(errorText = response.message)
                }
            }
        }
    }

    private fun fetchInitialSearch() {
        page = 1
        isQueryChanged = true
        viewModelScope.launch {
            fetchSearchResult()
        }
    }

    private suspend fun fetchSearchResult() {
        getSearchResults(page = page, keyword = query).collect { response ->
            when (response) {
                is Resource.Success -> {
                    val videoGameList = response.data.results
                    _videoGames.value =
                        if (isQueryChanged) videoGameList else _videoGames.value + videoGameList
                    _uiState.value = UiState.successState()
                }

                is Resource.Error -> {
                    _uiState.value = UiState.errorState(errorText = response.message)
                }
            }
        }
    }

}