package com.nalandya.videogamesapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nalandya.videogamesapp.databinding.ItemFavoriteGamesBinding
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame

class FavoriteVideoGamesAdapter(
    private val onItemClick: (id: Int) -> Unit,
    private val onRemoveClick: (item: FavoriteVideoGame) -> Unit
) : ListAdapter<FavoriteVideoGame, FavoriteVideoGamesAdapter.ViewHolder>(DiffCallback) {

    inner class ViewHolder private constructor(val view: ItemFavoriteGamesBinding) :
        RecyclerView.ViewHolder(view.root) {
        constructor(parent: ViewGroup) : this(
            ItemFavoriteGamesBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
        init {
            view.content.setOnClickListener {
                onItemClick(getItem(adapterPosition).id)
            }
            view.card.setOnClickListener {
                onItemClick(getItem(adapterPosition).id)
            }
            view.btnRemove.setOnClickListener {
                onRemoveClick(getItem(adapterPosition))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.favoriteVideoGame = getItem(position)
    }

    object DiffCallback : DiffUtil.ItemCallback<FavoriteVideoGame>() {
        override fun areItemsTheSame(
            oldItem: FavoriteVideoGame,
            newItem: FavoriteVideoGame
        ): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: FavoriteVideoGame,
            newItem: FavoriteVideoGame
        ): Boolean =
            oldItem == newItem
    }
}