package com.nalandya.videogamesapp.presentation.ui.favorite

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nalandya.videogamesapp.R
import com.nalandya.videogamesapp.databinding.FragmentFavoriteBinding
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.presentation.adapter.FavoriteVideoGamesAdapter
import com.nalandya.videogamesapp.presentation.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>(R.layout.fragment_favorite) {
    override val viewModel: FavoriteViewModel by viewModels()

    val adapterFavorites = FavoriteVideoGamesAdapter(::onItemClick, ::removeMovie)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        manageRecyclerViewAdapterLifecycle(binding.rvFavorites)
        collectFlows(listOf(::collectFavoriteMovies))
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchFavoriteMovies()
    }

    private fun onItemClick(id: Int) {
        val action = FavoriteFragmentDirections.actionToDetail(id)
        findNavController().navigate(action)
    }

    private fun removeMovie(favoriteVideoGame: FavoriteVideoGame) {
        viewModel.removeFavorite(favoriteVideoGame)
        showSnackbar(
            message = getString(R.string.snackbar_removed_item),
            actionText = getString(R.string.snackbar_action_undo),
            anchor = true
        ) {
            viewModel.addToFavorite(favoriteVideoGame)
        }
    }

    private suspend fun collectFavoriteMovies() {
        viewModel.favoriteVideoGames.collect { favoriteMovies ->
            adapterFavorites.submitList(favoriteMovies)
        }
    }


}