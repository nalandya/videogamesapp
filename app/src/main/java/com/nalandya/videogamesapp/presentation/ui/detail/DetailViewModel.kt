package com.nalandya.videogamesapp.presentation.ui.detail

import androidx.lifecycle.viewModelScope
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.model.VideoGameDetail
import com.nalandya.videogamesapp.domain.usecase.AddFavorite
import com.nalandya.videogamesapp.domain.usecase.CheckFavorite
import com.nalandya.videogamesapp.domain.usecase.DeleteFavorite
import com.nalandya.videogamesapp.domain.usecase.GetDetail
import com.nalandya.videogamesapp.presentation.ui.base.BaseViewModel
import com.nalandya.videogamesapp.util.Resource
import com.nalandya.videogamesapp.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getDetail: GetDetail,
    private val checkFavorite: CheckFavorite,
    private val deleteFavorite: DeleteFavorite,
    private val addFavorite: AddFavorite
) :
    BaseViewModel() {
    private var videoGamesId = -1
    private val _detailVideoGames = MutableStateFlow(VideoGameDetail.empty)
    val detailVideoGames get() = _detailVideoGames.asStateFlow()

    private val _isInFavorites = MutableStateFlow(false)
    val isInFavorites get() = _isInFavorites.asStateFlow()

    private lateinit var favoriteVideoGame: FavoriteVideoGame

    private fun checkFavorites() {
        viewModelScope.launch {
            _isInFavorites.value = checkFavorite(videoGamesId)
        }
    }

    private fun fetchDetails() {
        viewModelScope.launch {
            coroutineScope {
                getDetail.invoke(videoGamesId).collect { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data.apply {
                                _detailVideoGames.value = this
                                favoriteVideoGame = FavoriteVideoGame(
                                    id = id,
                                    name = name,
                                    released = released,
                                    rating = rating,
                                    image = image,
                                    date = System.currentTimeMillis()
                                )
                            }
                        }

                        is Resource.Error -> {
                            _uiState.value = UiState.errorState(errorText = response.message)
                        }
                    }
                }
            }
        }
    }

    fun updateFavorites() {
        viewModelScope.launch {
            if (_isInFavorites.value) {
                deleteFavorite(favoriteVideoGame)
                _isInFavorites.value = false
            } else {
                addFavorite(favoriteVideoGame)
                _isInFavorites.value = true
            }
        }
    }

    fun initRequest(videoGamesId: Int) {
        this.videoGamesId = videoGamesId
        fetchDetails()
        checkFavorites()
    }
}