package com.nalandya.videogamesapp.presentation.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nalandya.videogamesapp.R
import com.nalandya.videogamesapp.databinding.FragmentHomeBinding
import com.nalandya.videogamesapp.presentation.adapter.VideoGamesAdapter
import com.nalandya.videogamesapp.presentation.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) {
    override val viewModel: HomeViewModel by viewModels()

    val adapter = VideoGamesAdapter(::onItemClick)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        manageRecyclerViewAdapterLifecycle(binding.rvVideoGames)
        collectFlows(listOf(::collectVideoGames, ::collectUiState))
    }

    override fun onResume() {
        super.onResume()
        viewModel.initRequest()
    }

    private suspend fun collectVideoGames() {
        viewModel.videoGames.collect { videoGames ->
            adapter.submitList(videoGames)
        }
    }

    private fun onItemClick(id: Int) {
        val action = HomeFragmentDirections.actionToDetail(id)
        findNavController().navigate(action)
    }


    private suspend fun collectUiState() {
        viewModel.uiState.collect { state ->
            if (state.isError) showSnackbar(
                message = state.errorText!!,
                actionText = getString(R.string.button_retry)
            ) {
                viewModel.retryConnection {
                    viewModel.initRequest()
                }
            }
        }
    }

}