package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import javax.inject.Inject

class AddFavorite @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    suspend operator fun invoke(favoriteVideoGame: FavoriteVideoGame) {
        videoGamesRepository.insertFavoriteVideoGame(favoriteVideoGame)
    }
}