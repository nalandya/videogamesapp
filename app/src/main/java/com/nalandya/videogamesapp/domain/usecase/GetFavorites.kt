package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetFavorites @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    operator fun invoke(): Flow<List<FavoriteVideoGame>> = flow {
        emit(videoGamesRepository.getFavoriteVideoGame())
    }
}