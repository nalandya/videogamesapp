package com.nalandya.videogamesapp.domain.model

data class VideoGameList(
    val results: List<VideoGame>
) {
    companion object {
        val empty = VideoGameList(emptyList())
    }
}

data class VideoGame(
    val name: String? = null,
    val id: Int,
    val released: String? = null,
    val rating: Double? = null,
    val image: String? = null
)