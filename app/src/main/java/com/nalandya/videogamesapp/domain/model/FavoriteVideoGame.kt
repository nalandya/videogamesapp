package com.nalandya.videogamesapp.domain.model

data class FavoriteVideoGame(
    val name: String? = null,
    val id: Int,
    val released: String? = null,
    val rating: Double? = null,
    val image: String? = null,
    val date: Long
)
