package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import javax.inject.Inject

class CheckFavorite @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    suspend operator fun invoke(id: Int): Boolean = videoGamesRepository.videoGameExists(id)
}