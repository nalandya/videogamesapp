package com.nalandya.videogamesapp.domain.model

data class VideoGameDetail(
    val name: String? = null,
    val id: Int,
    val image: String? = null,
    val playtime: Int? = null,
    val released: String? = null,
    val publisher: String? = null,
    val description: String? = "",
    val rating: Double? = null
){
    companion object{
        val empty = VideoGameDetail(id= -1)
    }
}