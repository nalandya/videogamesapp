package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.model.VideoGameDetail
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import com.nalandya.videogamesapp.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetDetail @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    operator fun invoke(id: Int): Flow<Resource<VideoGameDetail>> = flow {
        emit(videoGamesRepository.getVideoGameDetail(id))
    }
}