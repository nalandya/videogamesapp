package com.nalandya.videogamesapp.domain.repository


import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.model.VideoGameDetail
import com.nalandya.videogamesapp.domain.model.VideoGameList
import com.nalandya.videogamesapp.util.Resource

interface VideoGamesRepository {
    suspend fun getVideoGameList(page: Int): Resource<VideoGameList>

    suspend fun getVideoGameDetail(id: Int): Resource<VideoGameDetail>

    suspend fun getVideoGameSearchResult(keyword: String, page: Int): Resource<VideoGameList>

    suspend fun getFavoriteVideoGame(): List<FavoriteVideoGame>

    suspend fun videoGameExists(id: Int): Boolean
    suspend fun insertFavoriteVideoGame(favoriteVideoGame: FavoriteVideoGame)
    suspend fun deleteFavoriteVideoGame(favoriteVideoGame: FavoriteVideoGame)
}