package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import javax.inject.Inject

class DeleteFavorite @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    suspend operator fun invoke(favoriteVideoGame: FavoriteVideoGame) {
        videoGamesRepository.deleteFavoriteVideoGame(favoriteVideoGame)
    }
}