package com.nalandya.videogamesapp.domain.usecase

import com.nalandya.videogamesapp.domain.model.VideoGameList
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import com.nalandya.videogamesapp.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetList @Inject constructor(private val videoGamesRepository: VideoGamesRepository) {
    operator fun invoke(page: Int): Flow<Resource<VideoGameList>> = flow {
        emit(videoGamesRepository.getVideoGameList(page))
    }
}