package com.nalandya.videogamesapp.di

import com.nalandya.videogamesapp.data.repository.VideoGamesRepositoryImpl
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun bindVideoGamesRepository(repository: VideoGamesRepositoryImpl): VideoGamesRepository
}