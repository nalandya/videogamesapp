package com.nalandya.videogamesapp.di

import android.content.Context
import androidx.room.Room
import com.nalandya.videogamesapp.data.local.dao.VideoGamesDao
import com.nalandya.videogamesapp.data.local.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideVideoGamesDao(appDatabase: AppDatabase): VideoGamesDao = appDatabase.videoGamesDao()

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "video-games-app-db").build()
}