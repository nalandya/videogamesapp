package com.nalandya.videogamesapp.data.repository

import com.nalandya.videogamesapp.data.local.dao.VideoGamesDao
import com.nalandya.videogamesapp.data.mapper.toFavoriteVideGameEntity
import com.nalandya.videogamesapp.data.mapper.toFavoriteVideoGame
import com.nalandya.videogamesapp.data.mapper.toVideoGameDetail
import com.nalandya.videogamesapp.data.mapper.toVideoGameList
import com.nalandya.videogamesapp.data.remote.api.VideoGamesApi
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame
import com.nalandya.videogamesapp.domain.model.VideoGameDetail
import com.nalandya.videogamesapp.domain.model.VideoGameList
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import com.nalandya.videogamesapp.util.Resource
import com.nalandya.videogamesapp.util.SafeApiCall
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VideoGamesRepositoryImpl @Inject constructor(
    private val api: VideoGamesApi,
    private val safeApiCall: SafeApiCall,
    private val videoGamesDao: VideoGamesDao
) : VideoGamesRepository {
    override suspend fun getVideoGameList(page: Int): Resource<VideoGameList> =
        safeApiCall.execute {
            api.getVideoGames(page).toVideoGameList()
        }

    override suspend fun getVideoGameDetail(id: Int): Resource<VideoGameDetail> =
        safeApiCall.execute {
            api.getVideoGameDetails(id).toVideoGameDetail()
        }

    override suspend fun getVideoGameSearchResult(
        keyword: String,
        page: Int
    ): Resource<VideoGameList> = safeApiCall.execute {
        api.getVideoGamesSearchResult(page = page, keyword = keyword).toVideoGameList()
    }

    override suspend fun getFavoriteVideoGame(): List<FavoriteVideoGame> =
        videoGamesDao.getAllVideoGames().map {
            it.toFavoriteVideoGame()
        }

    override suspend fun videoGameExists(id: Int): Boolean = videoGamesDao.videoGameExists(id)

    override suspend fun insertFavoriteVideoGame(favoriteVideoGame: FavoriteVideoGame) =
        videoGamesDao.insertVideoGames(favoriteVideoGame.toFavoriteVideGameEntity())

    override suspend fun deleteFavoriteVideoGame(favoriteVideoGame: FavoriteVideoGame) =
        videoGamesDao.deleteVideoGames(favoriteVideoGame.toFavoriteVideGameEntity())
}