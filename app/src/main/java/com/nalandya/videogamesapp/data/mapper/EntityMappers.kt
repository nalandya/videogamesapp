package com.nalandya.videogamesapp.data.mapper

import com.nalandya.videogamesapp.data.local.entitiy.FavoriteVideoGamesEntity
import com.nalandya.videogamesapp.domain.model.FavoriteVideoGame

fun FavoriteVideoGame.toFavoriteVideGameEntity() = FavoriteVideoGamesEntity(
    id = id,
    backgroundImage = image,
    name = name,
    rating = rating,
    released = released,
    date = date
)

fun FavoriteVideoGamesEntity.toFavoriteVideoGame() = FavoriteVideoGame(
    id = id,
    image = backgroundImage,
    name = name,
    rating = rating,
    released = released,
    date = date
)