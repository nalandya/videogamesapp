package com.nalandya.videogamesapp.data.local.entitiy

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteVideoGamesEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "rating") val rating: Double?,
    @ColumnInfo(name = "background_image") val backgroundImage: String?,
    @ColumnInfo(name = "released") val released: String?,
    @ColumnInfo(name = "date_added") val date: Long
)