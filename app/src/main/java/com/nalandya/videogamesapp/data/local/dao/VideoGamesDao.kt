package com.nalandya.videogamesapp.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nalandya.videogamesapp.data.local.entitiy.FavoriteVideoGamesEntity

@Dao
interface VideoGamesDao {
    @Query("SELECT * FROM favoritevideogamesentity ORDER BY date_added DESC")
    suspend fun getAllVideoGames(): List<FavoriteVideoGamesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVideoGames(videoGamesEntity: FavoriteVideoGamesEntity)

    @Delete
    suspend fun deleteVideoGames(videoGamesEntity: FavoriteVideoGamesEntity)

    @Query("SELECT EXISTS (SELECT * FROM favoritevideogamesentity WHERE id=:id)")
    suspend fun videoGameExists(id: Int): Boolean

}