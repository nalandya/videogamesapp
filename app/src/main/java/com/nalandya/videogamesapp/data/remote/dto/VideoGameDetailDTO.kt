package com.nalandya.videogamesapp.data.remote.dto


import com.google.gson.annotations.SerializedName

data class VideoGameDetailDTO(
    @SerializedName("achievements_count")
    val achievementsCount: Int? = null,
    @SerializedName("added")
    val added: Int? = null,
    @SerializedName("added_by_status")
    val addedByStatus: AddedByStatus? = null,
    @SerializedName("additions_count")
    val additionsCount: Int? = null,
    @SerializedName("alternative_names")
    val alternativeNames: List<String?>? = null,
    @SerializedName("background_image")
    val backgroundImage: String? = null,
    @SerializedName("background_image_additional")
    val backgroundImageAdditional: String? = null,
    @SerializedName("clip")
    val clip: Any? = null,
    @SerializedName("creators_count")
    val creatorsCount: Int? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("description_raw")
    val descriptionRaw: String? = null,
    @SerializedName("developers")
    val developers: List<Developer?>? = null,
    @SerializedName("dominant_color")
    val dominantColor: String? = null,
    @SerializedName("esrb_rating")
    val esrbRating: EsrbRating? = null,
    @SerializedName("game_series_count")
    val gameSeriesCount: Int? = null,
    @SerializedName("genres")
    val genres: List<Genre?>? = null,
    @SerializedName("id")
    val id: Int,
    @SerializedName("metacritic")
    val metacritic: Int? = null,
    @SerializedName("metacritic_platforms")
    val metacriticPlatforms: List<MetacriticPlatform?>? = null,
    @SerializedName("metacritic_url")
    val metacriticUrl: String? = null,
    @SerializedName("movies_count")
    val moviesCount: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("name_original")
    val nameOriginal: String? = null,
    @SerializedName("parent_achievements_count")
    val parentAchievementsCount: Int? = null,
    @SerializedName("parent_platforms")
    val parentPlatforms: List<ParentPlatform?>? = null,
    @SerializedName("parents_count")
    val parentsCount: Int? = null,
    @SerializedName("platforms")
    val platforms: List<Platform?>? = null,
    @SerializedName("playtime")
    val playtime: Int? = null,
    @SerializedName("publishers")
    val publishers: List<Publisher?>? = null,
    @SerializedName("rating")
    val rating: Double? = null,
    @SerializedName("rating_top")
    val ratingTop: Int? = null,
    @SerializedName("ratings")
    val ratings: List<Rating?>? = null,
    @SerializedName("ratings_count")
    val ratingsCount: Int? = null,
    @SerializedName("reactions")
    val reactions: Reactions? = null,
    @SerializedName("reddit_count")
    val redditCount: Int? = null,
    @SerializedName("reddit_description")
    val redditDescription: String? = null,
    @SerializedName("reddit_logo")
    val redditLogo: String? = null,
    @SerializedName("reddit_name")
    val redditName: String? = null,
    @SerializedName("reddit_url")
    val redditUrl: String? = null,
    @SerializedName("released")
    val released: String? = null,
    @SerializedName("reviews_count")
    val reviewsCount: Int? = null,
    @SerializedName("reviews_text_count")
    val reviewsTextCount: Int? = null,
    @SerializedName("saturated_color")
    val saturatedColor: String? = null,
    @SerializedName("screenshots_count")
    val screenshotsCount: Int? = null,
    @SerializedName("slug")
    val slug: String? = null,
    @SerializedName("stores")
    val stores: List<Store?>? = null,
    @SerializedName("suggestions_count")
    val suggestionsCount: Int? = null,
    @SerializedName("tags")
    val tags: List<Tag?>? = null,
    @SerializedName("tba")
    val tba: Boolean? = null,
    @SerializedName("twitch_count")
    val twitchCount: Int? = null,
    @SerializedName("updated")
    val updated: String? = null,
    @SerializedName("user_game")
    val userGame: Any? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("youtube_count")
    val youtubeCount: Int? = null
) {
    data class AddedByStatus(
        @SerializedName("beaten")
        val beaten: Int? = null,
        @SerializedName("dropped")
        val dropped: Int? = null,
        @SerializedName("owned")
        val owned: Int? = null,
        @SerializedName("playing")
        val playing: Int? = null,
        @SerializedName("toplay")
        val toplay: Int? = null,
        @SerializedName("yet")
        val yet: Int? = null
    )

    data class Developer(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class EsrbRating(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class Genre(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class MetacriticPlatform(
        @SerializedName("metascore")
        val metascore: Int? = null,
        @SerializedName("platform")
        val platform: Platform? = null,
        @SerializedName("url")
        val url: String? = null
    ) {
        data class Platform(
            @SerializedName("name")
            val name: String? = null,
            @SerializedName("platform")
            val platform: Int? = null,
            @SerializedName("slug")
            val slug: String? = null
        )
    }

    data class ParentPlatform(
        @SerializedName("platform")
        val platform: Platform? = null
    ) {
        data class Platform(
            @SerializedName("id")
            val id: Int? = null,
            @SerializedName("name")
            val name: String? = null,
            @SerializedName("slug")
            val slug: String? = null
        )
    }

    data class Platform(
        @SerializedName("platform")
        val platform: Platform? = null,
        @SerializedName("released_at")
        val releasedAt: String? = null,
        @SerializedName("requirements")
        val requirements: Requirements? = null
    ) {
        data class Platform(
            @SerializedName("games_count")
            val gamesCount: Int? = null,
            @SerializedName("id")
            val id: Int? = null,
            @SerializedName("image")
            val image: Any? = null,
            @SerializedName("image_background")
            val imageBackground: String? = null,
            @SerializedName("name")
            val name: String? = null,
            @SerializedName("slug")
            val slug: String? = null,
            @SerializedName("year_end")
            val yearEnd: Any? = null,
            @SerializedName("year_start")
            val yearStart: Int? = null
        )

        data class Requirements(
            @SerializedName("minimum")
            val minimum: String? = null,
            @SerializedName("recommended")
            val recommended: String? = null
        )
    }

    data class Publisher(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class Rating(
        @SerializedName("count")
        val count: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("percent")
        val percent: Double? = null,
        @SerializedName("title")
        val title: String? = null
    )

    data class Reactions(
        @SerializedName("1")
        val x1: Int? = null,
        @SerializedName("10")
        val x10: Int? = null,
        @SerializedName("11")
        val x11: Int? = null,
        @SerializedName("12")
        val x12: Int? = null,
        @SerializedName("13")
        val x13: Int? = null,
        @SerializedName("14")
        val x14: Int? = null,
        @SerializedName("15")
        val x15: Int? = null,
        @SerializedName("16")
        val x16: Int? = null,
        @SerializedName("18")
        val x18: Int? = null,
        @SerializedName("2")
        val x2: Int? = null,
        @SerializedName("20")
        val x20: Int? = null,
        @SerializedName("21")
        val x21: Int? = null,
        @SerializedName("3")
        val x3: Int? = null,
        @SerializedName("4")
        val x4: Int? = null,
        @SerializedName("5")
        val x5: Int? = null,
        @SerializedName("6")
        val x6: Int? = null,
        @SerializedName("7")
        val x7: Int? = null,
        @SerializedName("8")
        val x8: Int? = null,
        @SerializedName("9")
        val x9: Int? = null
    )

    data class Store(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("store")
        val store: Store? = null,
        @SerializedName("url")
        val url: String? = null
    ) {
        data class Store(
            @SerializedName("domain")
            val domain: String? = null,
            @SerializedName("games_count")
            val gamesCount: Int? = null,
            @SerializedName("id")
            val id: Int? = null,
            @SerializedName("image_background")
            val imageBackground: String? = null,
            @SerializedName("name")
            val name: String? = null,
            @SerializedName("slug")
            val slug: String? = null
        )
    }

    data class Tag(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("language")
        val language: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )
}