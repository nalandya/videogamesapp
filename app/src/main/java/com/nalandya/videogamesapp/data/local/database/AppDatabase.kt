package com.nalandya.videogamesapp.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nalandya.videogamesapp.data.local.dao.VideoGamesDao
import com.nalandya.videogamesapp.data.local.entitiy.FavoriteVideoGamesEntity

@Database(entities = [FavoriteVideoGamesEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun videoGamesDao(): VideoGamesDao
}