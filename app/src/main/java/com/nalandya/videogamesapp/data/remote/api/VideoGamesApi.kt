package com.nalandya.videogamesapp.data.remote.api

import com.nalandya.videogamesapp.data.remote.dto.VideoGameDetailDTO
import com.nalandya.videogamesapp.data.remote.dto.VideoGameListDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VideoGamesApi {
    @GET("games")
    suspend fun getVideoGames(@Query("page") page: Int): VideoGameListDTO

    @GET("games")
    suspend fun getVideoGamesSearchResult(
        @Query("page") page: Int,
        @Query("search") keyword: String
    ): VideoGameListDTO

    @GET("games/{id}")
    suspend fun getVideoGameDetails(@Path("id") id: Int): VideoGameDetailDTO
}