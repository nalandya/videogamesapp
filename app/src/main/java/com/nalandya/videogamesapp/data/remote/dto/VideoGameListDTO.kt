package com.nalandya.videogamesapp.data.remote.dto


import com.google.gson.annotations.SerializedName

data class VideoGameListDTO(
    @SerializedName("next")
    val next: String? = null,
    @SerializedName("results")
    val results: List<VideoGamesDTO>
)

data class VideoGamesDTO(
    @SerializedName("added")
    val added: Int? = null,
    @SerializedName("added_by_status")
    val addedByStatus: AddedByStatus? = null,
    @SerializedName("background_image")
    val backgroundImage: String? = null,
    @SerializedName("clip")
    val clip: Any? = null,
    @SerializedName("dominant_color")
    val dominantColor: String? = null,
    @SerializedName("esrb_rating")
    val esrbRating: EsrbRating? = null,
    @SerializedName("genres")
    val genres: List<Genre?>? = null,
    @SerializedName("id")
    val id: Int,
    @SerializedName("metacritic")
    val metacritic: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("playtime")
    val playtime: Int? = null,
    @SerializedName("rating")
    val rating: Double? = null,
    @SerializedName("rating_top")
    val ratingTop: Int? = null,
    @SerializedName("ratings")
    val ratings: List<Rating?>? = null,
    @SerializedName("ratings_count")
    val ratingsCount: Int? = null,
    @SerializedName("released")
    val released: String? = null,
    @SerializedName("reviews_count")
    val reviewsCount: Int? = null,
    @SerializedName("reviews_text_count")
    val reviewsTextCount: Int? = null,
    @SerializedName("saturated_color")
    val saturatedColor: String? = null,
    @SerializedName("short_screenshots")
    val shortScreenshots: List<ShortScreenshot?>? = null,
    @SerializedName("slug")
    val slug: String? = null,
    @SerializedName("stores")
    val stores: List<Store?>? = null,
    @SerializedName("suggestions_count")
    val suggestionsCount: Int? = null,
    @SerializedName("tags")
    val tags: List<Tag?>? = null,
    @SerializedName("tba")
    val tba: Boolean? = null,
    @SerializedName("updated")
    val updated: String? = null,
    @SerializedName("user_game")
    val userGame: Any? = null
) {
    data class AddedByStatus(
        @SerializedName("beaten")
        val beaten: Int? = null,
        @SerializedName("dropped")
        val dropped: Int? = null,
        @SerializedName("owned")
        val owned: Int? = null,
        @SerializedName("playing")
        val playing: Int? = null,
        @SerializedName("toplay")
        val toplay: Int? = null,
        @SerializedName("yet")
        val yet: Int? = null
    )

    data class EsrbRating(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class Genre(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )

    data class Rating(
        @SerializedName("count")
        val count: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("percent")
        val percent: Double? = null,
        @SerializedName("title")
        val title: String? = null
    )

    data class ShortScreenshot(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image")
        val image: String? = null
    )

    data class Store(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("store")
        val store: Store? = null
    ) {
        data class Store(
            @SerializedName("domain")
            val domain: String? = null,
            @SerializedName("games_count")
            val gamesCount: Int? = null,
            @SerializedName("id")
            val id: Int? = null,
            @SerializedName("image_background")
            val imageBackground: String? = null,
            @SerializedName("name")
            val name: String? = null,
            @SerializedName("slug")
            val slug: String? = null
        )
    }

    data class Tag(
        @SerializedName("games_count")
        val gamesCount: Int? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("image_background")
        val imageBackground: String? = null,
        @SerializedName("language")
        val language: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("slug")
        val slug: String? = null
    )
}