package com.nalandya.videogamesapp.data.mapper

import com.nalandya.videogamesapp.data.remote.dto.VideoGameDetailDTO
import com.nalandya.videogamesapp.data.remote.dto.VideoGameListDTO
import com.nalandya.videogamesapp.data.remote.dto.VideoGamesDTO
import com.nalandya.videogamesapp.domain.model.VideoGame
import com.nalandya.videogamesapp.domain.model.VideoGameDetail
import com.nalandya.videogamesapp.domain.model.VideoGameList

fun VideoGamesDTO.toVideoGames() =
    VideoGame(name = name, id = id, released = released, rating = rating, image = backgroundImage)

fun VideoGameListDTO.toVideoGameList() = VideoGameList(results.map { data -> data.toVideoGames() })

fun VideoGameDetailDTO.toVideoGameDetail() = VideoGameDetail(
    name = name,
    id = id,
    image = backgroundImage,
    playtime = playtime,
    released = released, publisher = publishers?.get(0)?.name,
    description = description,
    rating = rating
)