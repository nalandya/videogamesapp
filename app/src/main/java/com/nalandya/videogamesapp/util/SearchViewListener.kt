package com.nalandya.videogamesapp.util

interface SearchViewListener {
    fun onQueryTextSubmit(query:String)
    fun onClear()
}