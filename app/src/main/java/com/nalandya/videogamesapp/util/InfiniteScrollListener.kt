package com.nalandya.videogamesapp.util

interface InfiniteScrollListener {
    fun onLoadMore()
}