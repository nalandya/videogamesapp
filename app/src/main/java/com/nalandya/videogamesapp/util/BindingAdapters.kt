package com.nalandya.videogamesapp.util

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.nalandya.videogamesapp.R
import jp.wasabeef.glide.transformations.CropTransformation

@BindingAdapter("isVisible")
fun View.setVisibility(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("android:layout_width")
fun View.setWidth(width: Float) {
    layoutParams.width = width.toInt()
}

@BindingAdapter("android:layout_height")
fun View.setHeight(height: Float) {
    layoutParams.height = height.toInt()
}

@BindingAdapter("iconTint")
fun ImageView.setIconTint(color: Int?) {
    color?.let { setColorFilter(it.setTintColor()) }
}


@BindingAdapter("android:layout_marginBottom", "isImage", requireAll = false)
fun View.setLayoutMarginBottom(isGrid: Boolean, isImage: Boolean) {
    val params = layoutParams as ViewGroup.MarginLayoutParams
    params.bottomMargin = resources.getDimension(
        if (isGrid) {
            if (isImage) R.dimen.bottom_margin_small else R.dimen.bottom_margin
        } else R.dimen.zero_dp
    ).toInt()

    layoutParams = params
}

@BindingAdapter("android:background")
fun View.setBackground(color: Int) {
    setBackgroundColor(if (color != 0) color else ContextCompat.getColor(context, R.color.white))
}

@BindingAdapter("transparentBackground")
fun View.setTransparentBackground(backgroundColor: Int) {
    setBackgroundColor(ColorUtils.setAlphaComponent(backgroundColor, 220))
}

@BindingAdapter("isNested")
fun ViewPager2.handleNestedScroll(isNested: Boolean) {
    if (isNested) {
        val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
        recyclerViewField.isAccessible = true
        val recyclerView = recyclerViewField.get(this) as RecyclerView
        recyclerView.interceptTouch()
    }
}

@BindingAdapter("isNested")
fun RecyclerView.handleNestedScroll(isNested: Boolean) {
    if (isNested) interceptTouch()
}

@BindingAdapter("isGrid", "loadMore", "shouldLoadMore", requireAll = false)
fun RecyclerView.addInfiniteScrollListener(
    isGrid: Boolean,
    infiniteScroll: InfiniteScrollListener,
    shouldLoadMore: Boolean
) {
    if (shouldLoadMore) {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            private val layoutManagerType =
                if (isGrid) layoutManager as GridLayoutManager else layoutManager as LinearLayoutManager
            private val visibleThreshold = 10
            private var loading = true
            private var previousTotal = 0

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val visibleItemCount = layoutManagerType.childCount
                val totalItemCount = layoutManagerType.itemCount
                val firstVisibleItem = layoutManagerType.findFirstVisibleItemPosition()

                if (totalItemCount < previousTotal) previousTotal = 0

                if (loading && totalItemCount > previousTotal) {
                    loading = false
                    previousTotal = totalItemCount
                }

                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    infiniteScroll.onLoadMore()
                    loading = true
                }
            }
        })
    }
}

@BindingAdapter("fixedSize")
fun RecyclerView.setFixedSize(hasFixedSize: Boolean) {
    setHasFixedSize(hasFixedSize)
}

@SuppressLint("CheckResult")
@BindingAdapter("imageUrl", "centerCrop", "fitTop", requireAll = false)
fun ImageView.loadImage(
    posterPath: String?,
    centerCrop: Boolean?,
    fitTop: Boolean
) {

    val errorImage = AppCompatResources.getDrawable(
        context,
        R.drawable.ic_baseline_image_24
    )

    val glide = Glide.with(context)
        .load(posterPath)
        .error(errorImage)
        .skipMemoryCache(false)

    glide.into(this)
}

@BindingAdapter("searchViewListener")
fun setSearchViewOnQueryTextSubmitListener(
    searchView: SearchView,
    listener: SearchViewListener
) {
    searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            searchView.clearFocus()
            listener.onQueryTextSubmit(query ?: "")
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }

    })
    searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn).setOnClickListener {
        searchView.setQuery("",false)
        searchView.clearFocus()
        listener.onClear()
    }
}
