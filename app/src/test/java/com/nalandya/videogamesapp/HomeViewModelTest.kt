package com.nalandya.videogamesapp

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.damai.amarbankregistration.utils.InstantExecutorExtension
import com.nalandya.videogamesapp.domain.model.VideoGame
import com.nalandya.videogamesapp.domain.model.VideoGameList
import com.nalandya.videogamesapp.domain.repository.VideoGamesRepository
import com.nalandya.videogamesapp.domain.usecase.GetList
import com.nalandya.videogamesapp.domain.usecase.GetSearchResults
import com.nalandya.videogamesapp.presentation.ui.home.HomeViewModel
import com.nalandya.videogamesapp.util.Resource
import com.nalandya.videogamesapp.util.SafeApiCall
import com.nalandya.videogamesapp.util.UiState
import io.mockk.clearAllMocks
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
@ExtendWith(InstantExecutorExtension::class)
@MediumTest
class HomeViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var videoGamesRepository: VideoGamesRepository

    private lateinit var getList: GetList

    private lateinit var getSearchResults: GetSearchResults

    private lateinit var safeApiCall: SafeApiCall

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val mockContext = ApplicationProvider.getApplicationContext<Context>()
        getList = GetList(videoGamesRepository)
        getSearchResults = GetSearchResults(videoGamesRepository)
        safeApiCall = SafeApiCall(mockContext)
        viewModel = HomeViewModel(getList, getSearchResults)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    @DisplayName("Should reset page number and query change flag on initialization")
    fun initRequestResetsPageAndQueryChangeFlag() = runTest {
        runBlocking { viewModel.initRequest() }

        assertEquals(1, viewModel.page)
        assertEquals(true, viewModel.isQueryChanged)
    }

    @Test
    @DisplayName("Should set isQueryChanged to false on load more")
    fun onLoadMoreShouldSetIsQueryChangedToFalse() {
        viewModel.onLoadMore()

        assertFalse(viewModel.isQueryChanged)
    }

    @Test
    @DisplayName("Should increment page number on load more")
    fun onLoadMoreShouldIncrementPageNumber() {
        viewModel.onLoadMore()

        assertEquals(2, viewModel.page)
    }


    @Test
    @DisplayName("initRequest should fetch video games when query is empty")
    fun initRequestShouldFetchVideoGamesWhenQueryIsEmpty() = runTest {
        // Arrange
        val videoGameList =
            listOf(VideoGame(id = 1, name = "Game 1"), VideoGame(id = 2, name = "Game 2"))
        val resource = Resource.Success(VideoGameList(videoGameList))

        `when`(videoGamesRepository.getVideoGameList(1)).thenReturn(resource)

        // Act
        viewModel.initRequest()
        delay(2_000L)

        // Assert
        assertEquals(videoGameList, viewModel.videoGames.first())
        assertEquals(UiState.successState(), viewModel.uiState.value)
    }

    @Test
    @DisplayName("initRequest should fetch search results when query is not empty")
    fun initRequestShouldFetchSearchResultWhenQueryIsNotEmpty() = runTest {
        // Arrange
        val query = "example query"
        val videoGameList =
            listOf(VideoGame(id = 1, name = "Game 1"), VideoGame(id = 2, name = "Game 2"))
        val resource = Resource.Success(VideoGameList(videoGameList))
        `when`(videoGamesRepository.getVideoGameSearchResult(page = 1, keyword = query)).thenReturn(
            resource
        )

        // Act
        viewModel.query = query
        viewModel.initRequest()
        delay(2_000L)

        // Assert
        assertEquals(videoGameList, viewModel.videoGames.first())
        assertEquals(UiState.successState(), viewModel.uiState.value)
    }
}